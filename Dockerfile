FROM python:3.9.10-slim-buster

RUN mount -t tmpfs -o rw,nosuid,nodev,noexec,relatime,size=2048M tmpfs /dev/shm

RUN apt-get update
RUN apt-get -y install curl wget unzip gnupg2
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
RUN apt-get update && apt-get -y install google-chrome-stable

RUN wget https://chromedriver.storage.googleapis.com/2.41/chromedriver_linux64.zip
RUN unzip chromedriver_linux64.zip
ENV driver_path="/usr/bin/chromedriver"
RUN mv chromedriver $driver_path
RUN chmod +x $driver_path
RUN $driver_path -v

RUN apt-get install apt-transport-https

RUN apt-get update && apt-get install -y alien libaio1 wget
RUN wget http://yum.oracle.com/repo/OracleLinux/OL7/oracle/instantclient/x86_64/getPackage/oracle-instantclient19.3-basiclite-19.3.0.0.0-1.x86_64.rpm
RUN alien -i --scripts oracle-instantclient*.rpm
RUN rm -f oracle-instantclient19.3*.rpm && apt-get -y autoremove && apt-get -y clean
RUN ls /usr/lib/oracle/19.3
RUN ls /usr/lib/oracle/19.3/client64
RUN ls /usr/lib/oracle/19.3/client64/lib

COPY requirements.txt .

RUN python3 -m pip install cx_Oracle --upgrade
RUN pip3 install -r requirements.txt
